
OVERVIEW

The countdowmtimer module provides a timer implemented through javascipt
which gives you a dynamic countdown (second-by-second) to a certain date 
and time in the future.  It uses the onload event then dynamically searches
content/blocks/teasers for certain css classes and then injects itself.

-------------- INSTALLING -----------------------------------------

countdowmtimer can be installed simply by activating the module.
There are no module dependencies.
There are no table components.

-------------- USAGE ----------------------------------------------

Timer in block:
1. set input format to php code
2. create a new block with 
   -a span with a class="countdowntimer"
   -correctly formatted date and/or time.

example:
<?php 
drupal_add_js('countdowntimer/timerjs');
?>
<b>Countdown to whatever...</b>
<span class="countdowntimer">2007-10-7</span>


Timer in a node:
1. just add the html (input format, if filtered must allow span tags).

example:
<b>Countdown to whatever...</b>
<span class="countdowntimer">2007-10-15 20:30:00 -8</span>

-------------- IMPORTANT -------------------------------------------
DATE FORMAT ( yyyy-mm-dd hh:mm:ss tz_offset)
SHOULD BE LIKE THESE EXAMPLES

2007
2007-10
2007-10-15 20
2007-10-15 20:30
2007-10-15 20:30:00
2007-10-15 20:30:00 -8

defaults will be used if you don't fill in all of the fields

-------------- OUTPUT FORMAT ---------------------------------------
The display of the actual timer is configurable in the site building 
admin menu: countdowntimer.

Currently supported replacement values are:
%day%		-	Day number of target date (0-31)
%month%		-	Month number of target date (1-12)
%year%		-   Year number of target date (4 digit number)
%dow%		-	Day-Of-Week (Mon-Sun)
%moy%		-	Month-Of-Year (Jan-Dec)
%days%		-	Days left countdown field (number)
%hours%		-	Hours left countdown field (number)
%mins%		-   Minutes left countdown field (number)
%secs%		-	Seconds left countdown field (number)

-------------- CAVEATS ---------------------------------------------
If a daylight saving time shift should occur in either the client's tz or
the target's tz between the current date/time and your target datetime,
you could be off by one hour until you pass the point of conversion.
